// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCoi8BiBMCAxzJy7BVaYhiGmHQMuN0Js5E",
    authDomain: "noticeapp-3d49e.firebaseapp.com",
    databaseURL: "https://noticeapp-3d49e.firebaseio.com",
    projectId: "noticeapp-3d49e",
    storageBucket: "noticeapp-3d49e.appspot.com",
    messagingSenderId: "46058547675"
  }
};
