export interface Item {
  //auto generated
  id:string;
  uid?:string;
  user?:string;
  date?: string;
  timestamp?:object;
  photoURL?:any;
  //generated from inputs
  title?:string;
  location?:string;
  phone?:string;
  description?: string;
  price?: number;
  brand?: string;
  category?: string;
  condition?: string;
}
