import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { EmailComponent } from './components/email/email.component';
import { SignupComponent } from './components/signup/signup.component';
import { AuthGuard } from './services/auth.service';
import { routes } from './app.routes';
import { WallComponent } from './components/wall/wall.component';
import { AddComponent } from './components/add/add.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { environment } from '../environments/environment';
import { ItemService } from './services/item.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { ToasterService } from './services/toaster.service';
import { UserWallComponent } from './components/user.wall/user.wall.component';
import { NoticeComponent } from './components/notice/notice.component';
import { AgmCoreModule } from '@agm/core';
import { FooterComponent } from './components/footer/footer.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmailComponent,
    SignupComponent,
    WallComponent,
    AddComponent,
    NavbarComponent,
    LoadingSpinnerComponent,
    UserWallComponent,
    NoticeComponent,
    FooterComponent
  ],


  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    routes,
    BrowserAnimationsModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCBXHlgYYAzPPhLIQ1qWMxBp0uBLjbpF28',
      libraries: ['places']
    }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [AuthGuard, ItemService, ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
