import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Item } from '../Item';
import * as firebase from 'firebase';

@Injectable()
export class ItemService {
  itemsCollection: AngularFireList<Item>;
  items: Observable<Item[]>;
  item: Observable<Item>;

  constructor(private afs: AngularFireDatabase) {
    this.itemsCollection = this.afs.list('items', ref => ref.orderByChild('timestamp'));
    this.items = this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
  }

  getItems(): Observable<Item[]>{
    return this.items;
  }

  getItem(id: string): Observable<Item>{
    return this.getItems().map(items => items.find(item => item.id === id));
  }

  addItem(item: Item){
    this.itemsCollection.push(item).then(()=>console.log('Notice added'));
  }

  deleteItem(key: string, user: string, id: string){
    //remove item from collection
    this.itemsCollection.remove(key);
    //remove image from storage
    const storageRef = firebase.storage().ref('images/'+user+'/'+id);
    storageRef.delete().then(function() {}).catch(function(error) {
      console.error(error);
    });
  }

}
