import { Injectable } from '@angular/core';
declare var toastr:any;
@Injectable()
export class ToasterService {

  constructor() { }

  Success(title:string, msg?:string){
    toastr.success(title,msg);
  }

  Warning(title:string, msg?:string){
    toastr.warning(title,msg);
  }

  Info(title:string, msg?:string){
    toastr.info(title,msg);
  }

}
