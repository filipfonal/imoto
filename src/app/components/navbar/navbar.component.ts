import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { ToasterService } from '../../services/toaster.service';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../../router.animations';
import { Observable } from 'rxjs/Rx';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  private userLoggedIn = null;
  private userName;
  private photoURL;


  constructor(private af: AngularFireAuth, private router: Router, private toaster: ToasterService, private translate: TranslateService) {
    translate.setDefaultLang('pl');
    this.af.authState.subscribe((auth) => {
      this.userLoggedIn = auth;
      this.userName = auth.email;
      this.photoURL = auth.photoURL;
    });
  }

  switchPolish() {
    this.translate.use('pl');
  }

  switchEnglish() {
    this.translate.use('en');
  }

  login() {
     this.router.navigateByUrl('/login');
  }

  addNotice() {
     this.router.navigateByUrl('/add');
  }

  userNotice() {
     this.router.navigateByUrl('/user');
  }

  logout() {
    this.af.auth.signOut();
    this.toaster.Info((this.translate.currentLang !== 'en') ? 'Wylogowano pomyślnie!' : 'Logout successful!');
    this.router.navigateByUrl('/');
    console.log('logged out');
  }

  ngOnInit() {
  }

  isActive(viewLocation) {
    return viewLocation === location.pathname;
  }

}
