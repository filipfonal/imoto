import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { ItemService } from '../../services/item.service';
import { ToasterService } from '../../services/toaster.service';
import { Item } from '../../Item';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.less']
})

export class WallComponent implements OnInit {
  protected items: Item[];
  protected showSpinner = true;
  protected userLoggedIn;
  protected userID;

  constructor(protected itemService: ItemService, protected af: AngularFireAuth, protected toaster: ToasterService,
    protected translate: TranslateService){
    this.af.authState.subscribe((auth) => {
      this.userLoggedIn = auth;
      this.userID = auth.uid;
    });
  }

  ngOnInit() {
    this.itemService.getItems().subscribe(items => {
      this.items = items;
      this.showSpinner = false;
    });
  }

  deleteItem(key: string, user: string, id: string) {
    if (window.confirm((this.translate.currentLang !== 'en') ? 'Czy napewno chcesz usunąć?' : 'Are you sure you want to delete?')){
      this.itemService.deleteItem(key, user, id);
      this.toaster.Warning((this.translate.currentLang !== 'en') ? 'Ogłoszenie zostało usunięte!' : 'The advertisment has been deleted!');
    }
  }

}
