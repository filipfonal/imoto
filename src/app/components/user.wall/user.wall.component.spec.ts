import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { User.WallComponent } from './user.wall.component';

describe('User.WallComponent', () => {
  let component: User.WallComponent;
  let fixture: ComponentFixture<User.WallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ User.WallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(User.WallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
