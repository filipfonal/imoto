import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { ItemService } from '../../services/item.service';
import { WallComponent } from '../wall/wall.component';
import { ToasterService } from '../../services/toaster.service';
import { Item } from '../../Item';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-wall',
  templateUrl: './user.wall.component.html',
  styleUrls: ['./user.wall.component.less']
})

export class UserWallComponent extends WallComponent implements OnInit {

  constructor(protected itemService: ItemService, protected af: AngularFireAuth,
    protected toaster: ToasterService, protected translate: TranslateService){
    super(itemService, af, toaster, translate);
  }

  ngOnInit() {
    this.itemService.getItems().subscribe(items => {
      this.items = items.filter((el)=>el.uid === this.userID);
      this.showSpinner = false;
    });
    window.scrollTo(0,0);
  }

}
