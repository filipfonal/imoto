import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../services/item.service';
import { ToasterService } from '../../services/toaster.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { Item } from '../../Item';
import { brands } from '../../brands';
import { MapsAPILoader } from '@agm/core';
import { } from '@types/googlemaps';
import { ViewChild, ElementRef, NgZone } from '@angular/core';
import * as firebase from 'firebase';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})
export class AddComponent implements OnInit {

  @ViewChild('search') public searchElement: ElementRef;

  private item: Item = {
    id : this.generateID(),
    uid : '',
    title: '',
    user: '',
    location: '',
    phone: '',
    description: '',
    date: '',
    timestamp: null,
    photoURL: '',
    price: null,
    brand: '',
    category: '',
    condition: ''
  };

  private photoURL = '';
  private userEmail;
  private userID;
  private file: File;
  private metaData;
  private progress;
  private formSubmit = false;
  private brands = brands;
  private autocomplete;

  constructor(
    private itemService: ItemService,
    private af: AngularFireAuth,
    private router: Router,
    private toaster: ToasterService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private translate: TranslateService) {
      this.af.authState.subscribe((auth) => {
        this.userEmail = auth.email;
        this.userID = auth.uid;
      });
  }


  ngOnInit() {
    window.scrollTo(0, 0);
    this.mapsAPILoader.load().then(
      () => {
        this.autocomplete = new google.maps.places.Autocomplete(
          this.searchElement.nativeElement,
          { types: ['geocode'] }
        );

        this.autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            const place: google.maps.places.PlaceResult = this.autocomplete.getPlace();
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
          });
        });
      }
    );
  }

  onSubmit(){
    if(this.validate()){
      if(this.file != null){
        this.uploadImage();
      }else{
        this.add();
      }
    }
  }

  add(){
    if (this.autocomplete.getPlace() !== undefined) {
      this.item.location = this.autocomplete.getPlace().name;
    }
    this.item.timestamp = firebase.database.ServerValue.TIMESTAMP;
    this.item.date = new Date().toLocaleString();
    this.item.uid = this.userID;
    this.item.user = this.userEmail;
    this.item.photoURL = this.photoURL;

    this.itemService.addItem(this.item);
    this.router.navigateByUrl('');
    this.success((this.translate.currentLang !== 'en') ? 'Dodano ogłoszenie!' : 'Advertisement added!');
  }

  selectedImage(event: any) {
    this.file = event.target.files[0];
    this.metaData = {'contentType': this.file.type};
  }

  uploadImage() {
      document.getElementById('btnSubmit').setAttribute('disabled', 'disabled');

      const storageRef = firebase.storage().ref('images/' + this.userEmail + '/' + this.item.id);
      const uploadTask = storageRef.put(this.file, this.metaData);
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot) => {
          this.progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
        }, (error) => {
          console.error(error.message);
        }, () => {
        this.photoURL = uploadTask.snapshot.downloadURL;
        document.getElementById('btnSubmit').removeAttribute('disabled');
        this.add();
      });
  }

  success(msg: string) {
    this.toaster.Success(msg);
  }

  generateID() {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  validate(): boolean {
    return this.item.title !== ''      &&
           this.item.title.length > 2  &&
           this.item.phone !== ''      &&
           this.item.location !== ''   &&
           this.item.price !== null    &&
           this.item.phone !== ''      &&
           this.item.phone.length >= 7;
  }
}
