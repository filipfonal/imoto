import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './services/auth.service';
import { SignupComponent } from './components/signup/signup.component';
import { EmailComponent } from './components/email/email.component';
import { WallComponent } from './components/wall/wall.component';
import { AddComponent } from './components/add/add.component';
import { UserWallComponent } from './components/user.wall/user.wall.component';
import { NoticeComponent } from './components/notice/notice.component';

export const router: Routes = [
    { path: '', component: WallComponent },
    { path: 'login', component: LoginComponent },
    { path: 'add', component: AddComponent, canActivate: [AuthGuard]},
    { path: 'user', component: UserWallComponent, canActivate: [AuthGuard]},
    { path: 'signup', component: SignupComponent },
    { path: 'login-email', component: EmailComponent },
    { path: 'notice/:id', component: NoticeComponent }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
